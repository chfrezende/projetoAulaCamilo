import React, {useState} from "react";

const Contato = () =>
{
    const [objeto, setObjeto] = useState({nome :'Ana', email : "ana@email.com"});
    const alterarPropriedade = (propNome, propValor) =>{
        let novoObjeto = {...objeto};
        novoObjeto[propNome] = propValor;
        setObjeto(novoObjeto);
    }
    return (
        <div>
            <label htmlFor="nome" >
                Nome
            </label>
            <input id ="nome" 
            type = "text" 
            value={objeto.nome} 
            onChange = {(e) => alterarPropriedade(e.target.id, e.target.value)}></input>
            <label htmlFor="email" >
                email
            </label>
            <input id ="email" 
            type = "text" 
            value={objeto.email} 
            onChange = {(e) => alterarPropriedade(e.target.id, e.target.value)}></input>
        </div>
        
    );

};
export default Contato;